ARCH:=mips
SUBTARGET:=VANT5
BOARDNAME:=VANT-5
BRCM_CHIP:=63268

define Target/Description
	Broadcom 63x68 (VANT-5)
endef

IMAGE_CONF_FILE=vant-5.conf
LER_CONF_FILE=vant-5_ler.conf

