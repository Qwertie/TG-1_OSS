#
# Copyright (C) 2010 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

define KernelPackage/bcm63xx-tch-chipinfo
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom Chip Info driver
  DEPENDS:=@TARGET_brcm63xx_tch
  KCONFIG:=CONFIG_BCM_CHIPINFO
  FILES:=$(LINUX_DIR)/../../bcmdrivers/broadcom/char/chipinfo/bcm9$(BRCM_CHIP)/chipinfo.ko
  AUTOLOAD:=$(call AutoLoad,50,chipinfo)
endef

$(eval $(call KernelPackage,bcm63xx-tch-chipinfo))

define KernelPackage/bcm63xx-tch-ingqos
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom Ingress QoS driver
  DEPENDS:=@TARGET_brcm63xx_tch
  KCONFIG:=CONFIG_BCM_INGQOS
  FILES:=$(LINUX_DIR)/../../bcmdrivers/broadcom/char/ingqos/bcm9$(BRCM_CHIP)/bcm_ingqos.ko
  AUTOLOAD:=$(call AutoLoad,51,bcm_ingqos)
endef

$(eval $(call KernelPackage,bcm63xx-tch-ingqos))


define KernelPackage/bcm63xx-tch-bpm
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom BPM driver
  DEPENDS:=@TARGET_brcm63xx_tch
  KCONFIG:=CONFIG_BCM_BPM CONFIG_BCM_BPM_BUF_MEM_PRCNT=15
  FILES:=$(LINUX_DIR)/../../bcmdrivers/broadcom/char/bpm/bcm9$(BRCM_CHIP)/bcm_bpm.ko
  AUTOLOAD:=$(call AutoLoad,52,bcm_bpm)
endef

$(eval $(call KernelPackage,bcm63xx-tch-bpm))


define KernelPackage/bcm63xx-tch-pktflow
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom Packet Flow driver
  DEPENDS:=@TARGET_brcm63xx_tch
  KCONFIG:=CONFIG_BCM_PKTFLOW
  FILES:=$(LINUX_DIR)/../../bcmdrivers/broadcom/char/pktflow/bcm9$(BRCM_CHIP)/pktflow.ko
  AUTOLOAD:=$(call AutoLoad,53,pktflow)
endef

$(eval $(call KernelPackage,bcm63xx-tch-pktflow))


define KernelPackage/bcm63xx-tch-fap
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom FAP driver
  DEPENDS:=@TARGET_brcm63xx_tch
  KCONFIG:=CONFIG_BCM_FAP CONFIG_BCM_FAP_LAYER2=n CONFIG_BCM_FAP_GSO=y CONFIG_BCM_FAP_GSO_LOOPBACK=y CONFIG_BCM_FAP_IPV6=y
  FILES:=$(LINUX_DIR)/../../bcmdrivers/broadcom/char/fap/bcm9$(BRCM_CHIP)/bcmfap.ko
  AUTOLOAD:=$(call AutoLoad,54,bcmfap)
endef

$(eval $(call KernelPackage,bcm63xx-tch-fap))


define KernelPackage/bcm63xx-tch-enet
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom Ethernet driver
ifdef CONFIG_TARGET_brcm63xx_tch_HG1XEWAN
  DEPENDS:=@TARGET_brcm63xx_tch +kmod-bcm63xx-tch-pktflow +kmod-bcm63xx-tch-pktrunner +kmod-bcm63xx-tch-bdmf +kmod-bcm63xx-tch-rdpa +TARGET_brcm63xx_tch:bdmf_shell +TARGET_brcm63xx_tch:swmdk
else
  DEPENDS:=@TARGET_brcm63xx_tch +kmod-bcm63xx-tch-pktflow
endif
  KCONFIG:=CONFIG_BCM_ENET
  FILES:=$(LINUX_DIR)/../../bcmdrivers/opensource/net/enet/bcm9$(BRCM_CHIP)/bcm_enet.ko
  AUTOLOAD:=$(call AutoLoad,55,bcm_enet)
endef

$(eval $(call KernelPackage,bcm63xx-tch-enet))

define KernelPackage/bcm63xx-tch-bdmf
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom BDMF driver
  DEPENDS:=@TARGET_brcm63xx_tch
  KCONFIG:=CONFIG_BCM_BDMF
  FILES:=$(LINUX_DIR)/../../bcmdrivers/opensource/char/bdmf/bcm9$(BRCM_CHIP)/bdmf.ko
  AUTOLOAD:=$(call AutoLoad,50,bdmf)
endef

$(eval $(call KernelPackage,bcm63xx-tch-bdmf))

define KernelPackage/bcm63xx-tch-rdpa
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom RDPA driver
  DEPENDS:=@TARGET_brcm63xx_tch +kmod-bcm63xx-tch-rdpa-gpl
  KCONFIG:=CONFIG_BCM_RDPA
  FILES:=$(LINUX_DIR)/../../bcmdrivers/broadcom/char/rdpa/bcm9$(BRCM_CHIP)/rdpa.ko
  AUTOLOAD:=$(call AutoLoad,52,rdpa)
endef

$(eval $(call KernelPackage,bcm63xx-tch-rdpa))

define KernelPackage/bcm63xx-tch-rdpa-mw
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom RDPA Middleware driver
  DEPENDS:=@TARGET_brcm63xx_tch +kmod-bcm63xx-tch-rdpa-gpl +kmod-bcm63xx-tch-bdmf
  KCONFIG:=CONFIG_BCM_RDPA_MW
  FILES:=$(LINUX_DIR)/../../bcmdrivers/opensource/char/rdpa_mw/bcm9$(BRCM_CHIP)/rdpa_mw.ko
  AUTOLOAD:=$(call AutoLoad,52,rdpa_mw)
endef

$(eval $(call KernelPackage,bcm63xx-tch-rdpa-mw))

define KernelPackage/bcm63xx-tch-rdpa-gpl
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom RDPA GPL driver
  DEPENDS:=@TARGET_brcm63xx_tch
  KCONFIG:=CONFIG_BCM_RDPA_GPL
  FILES:=$(LINUX_DIR)/../../bcmdrivers/opensource/char/rdpa_gpl/bcm9$(BRCM_CHIP)/rdpa_gpl.ko
  AUTOLOAD:=$(call AutoLoad,51,rdpa_gpl)
endef

$(eval $(call KernelPackage,bcm63xx-tch-rdpa-gpl))

define KernelPackage/bcm63xx-tch-rdpa-drv
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom RDPA driver
  DEPENDS:=@TARGET_brcm63xx_tch
  KCONFIG:=CONFIG_BCM_RDPA_DRV
ifdef CONFIG_PACKAGE_kmod-brcm-4.16L.02_voice4.16L.01
  FILES:=$(LINUX_DIR)/../../bcmdrivers/opensource/char/rdpa_drv/bcm9$(BRCM_CHIP)/rdpa_cmd_drv.ko \
         $(LINUX_DIR)/../../bcmdrivers/opensource/char/rdpa_drv/bcm9$(BRCM_CHIP)/rdpa_cmd_spdsvc.ko \
         $(LINUX_DIR)/../../bcmdrivers/opensource/char/rdpa_drv/bcm9$(BRCM_CHIP)/rdpa_cmd_tm.ko
else
  FILES:=$(LINUX_DIR)/../../bcmdrivers/opensource/char/rdpa_drv/bcm9$(BRCM_CHIP)/rdpa_cmd_drv.ko \
         $(LINUX_DIR)/../../bcmdrivers/opensource/char/rdpa_drv/bcm9$(BRCM_CHIP)/rdpa_cmd_spdsvc.ko \
         $(LINUX_DIR)/../../bcmdrivers/opensource/char/rdpa_drv/bcm9$(BRCM_CHIP)/rdpa_cmd_ds_wan_udp_filter.ko \
         $(LINUX_DIR)/../../bcmdrivers/opensource/char/rdpa_drv/bcm9$(BRCM_CHIP)/rdpa_cmd_tm.ko
endif
  AUTOLOAD:=
endef

$(eval $(call KernelPackage,bcm63xx-tch-rdpa-drv))


define KernelPackage/bcm63xx-tch-pktrunner
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom Packet Runner
  DEPENDS:=@TARGET_brcm63xx_tch
  KCONFIG:=CONFIG_BCM_PKTRUNNER
  FILES:=$(LINUX_DIR)/../../bcmdrivers/broadcom/char/pktrunner/bcm9$(BRCM_CHIP)/pktrunner.ko
#  AUTOLOAD:=$(call AutoLoad,53,pktrunner)
endef

$(eval $(call KernelPackage,bcm63xx-tch-pktrunner))

define KernelPackage/bcm63xx-tch-vlan
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom VLAN driver
  DEPENDS:=@TARGET_brcm63xx_tch
  KCONFIG:=CONFIG_BCM_VLAN
  FILES:=$(LINUX_DIR)/../../bcmdrivers/broadcom/char/vlan/bcm9$(BRCM_CHIP)/bcmvlan.ko
  AUTOLOAD:=$(call AutoLoad,56,bcmvlan)
endef

$(eval $(call KernelPackage,bcm63xx-tch-vlan))

define KernelPackage/bcm63xx-tch-wireless
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom Wireless driver
  DEPENDS:=@TARGET_brcm63xx_tch +kmod-bcm63xx-tch-enet +(PACKAGE_kmod-brcm-4.16L.02_voice4.16L.01):kmod-bcm63xx-tch-wireless-emf  +(PACKAGE_kmod-brcm-4.16L.02A):kmod-bcm63xx-tch-wireless-emf +(PACKAGE_kmod-brcm-4.16L.03):kmod-bcm63xx-tch-wireless-emf
  KCONFIG:=CONFIG_BCM_WLAN CONFIG_BCM_WLALTBLD="ap_2nv"
  FILES:=$(LINUX_DIR)/../../bcmdrivers/broadcom/net/wl/bcm9$(BRCM_CHIP)/build/wlobj-wlconfig_lx_wl_dslcpe_pci_ap_2nv-kdb/wl.ko
  AUTOLOAD:=$(call AutoLoad,57,wl)
endef

#Common nvram vars
define KernelPackage/bcm63xx-tch-wireless/install
	$(INSTALL_DIR) $(1)/etc/wlan_common
#For impl18 and higher
ifneq ($(wildcard $(LINUX_DIR)/../../bcmdrivers/broadcom/net/wl/bcm9$(BRCM_CHIP)/build/wlobj-wlconfig_lx_wl_dslcpe_pci_ap_2nv-kdb/shared/bcmcmn_nvramvars.bin),)
	$(INSTALL_DATA) $(LINUX_DIR)/../../bcmdrivers/broadcom/net/wl/bcm9$(BRCM_CHIP)/build/wlobj-wlconfig_lx_wl_dslcpe_pci_ap_2nv-kdb/shared/bcmcmn_nvramvars.bin $(1)/etc/wlan_common/bcmcmn_nvramvars.bin
else
	$(INSTALL_DATA) $(LINUX_DIR)/../../bcmdrivers/broadcom/net/wl/bcm9$(BRCM_CHIP)/build/wlobj-wlconfig_lx_wl_dslcpe_pci_ap_2nv-kdb/shared/nvramvars_cmn.bin $(1)/etc/wlan_common/bcmcmn_nvramvars.bin
endif
endef

$(eval $(call KernelPackage,bcm63xx-tch-wireless))

define KernelPackage/bcm63xx-tch-wireless-emf
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom Wireless EMF driver
  DEPENDS:=@PACKAGE_kmod-brcm-4.16L.02_voice4.16L.01||@PACKAGE_kmod-brcm-4.16L.02A||@PACKAGE_kmod-brcm-4.16L.03
  FILES:=$(LINUX_DIR)/../../bcmdrivers/broadcom/net/wl/bcm9$(BRCM_CHIP)/emf/wlemf.ko
  AUTOLOAD:=$(call AutoLoad,56,wlemf)
endef

$(eval $(call KernelPackage,bcm63xx-tch-wireless-emf))

define KernelPackage/bcm63xx-tch-wireless-wlcsm
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom Wireless WLCSM driver
  DEPENDS:=@PACKAGE_kmod-brcm-4.16L.02_voice4.16L.01||@PACKAGE_kmod-brcm-4.16L.02A||@PACKAGE_kmod-brcm-4.16L.03
ifdef CONFIG_PACKAGE_kmod-brcm-4.16L.03
  FILES:=$(LINUX_DIR)/../../bcmdrivers/broadcom/net/wl/impl20/wl/wlcsm_ext/wlcsm.ko
else
  FILES:=$(LINUX_DIR)/../../bcmdrivers/broadcom/net/wl/impl18/wl/wlcsm_ext/wlcsm.ko
endif
  AUTOLOAD:=$(call AutoLoad,56,wlcsm)
endef

$(eval $(call KernelPackage,bcm63xx-tch-wireless-wlcsm))

define KernelPackage/bcm63xx-tch-wireless-dhd
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom Wireless driver (offloaded)
  DEPENDS:=@PACKAGE_kmod-brcm-4.16L.02_voice4.16L.01||@PACKAGE_kmod-brcm-4.16L.02A||@PACKAGE_kmod-brcm-4.16L.03 +kmod-bcm63xx-tch-enet +kmod-bcm63xx-tch-wireless-emf
  KCONFIG:=CONFIG_BCM_WLAN CONFIG_BCM_WLALTBLD="ap_2nv"
  FILES:=$(LINUX_DIR)/../../bcmdrivers/broadcom/net/wl/bcm9$(BRCM_CHIP)/dhd/build/dhdobj-dhdconfig_lx_dhd_dslcpe_pci_ap_2nv-kdb/dhd.ko
  AUTOLOAD:=$(call AutoLoad,58,dhd)
endef

#DHD firmware + common nvram vars
define KernelPackage/bcm63xx-tch-wireless-dhd/install
	$(INSTALL_DIR) $(1)/etc/wlan_common
#For impl18 and higher
ifneq ($(wildcard $(LINUX_DIR)/../../bcmdrivers/broadcom/net/wl/bcm9$(BRCM_CHIP)/build/wlobj-wlconfig_lx_wl_dslcpe_pci_ap_2nv-kdb/shared/bcmcmn_nvramvars.bin),)
	$(INSTALL_DATA) $(LINUX_DIR)/../../bcmdrivers/broadcom/net/wl/bcm9$(BRCM_CHIP)/build/wlobj-wlconfig_lx_wl_dslcpe_pci_ap_2nv-kdb/shared/bcmcmn_nvramvars.bin $(1)/etc/wlan_common/bcmcmn_nvramvars.bin
else
	$(INSTALL_DATA) $(LINUX_DIR)/../../bcmdrivers/broadcom/net/wl/bcm9$(BRCM_CHIP)/build/wlobj-wlconfig_lx_wl_dslcpe_pci_ap_2nv-kdb/shared/nvramvars_cmn.bin $(1)/etc/wlan_common/bcmcmn_nvramvars.bin
endif
	$(INSTALL_DIR) $(1)/etc/wlan_dhd
	$(INSTALL_DIR) $(1)/etc/wlan_dhd/43602a1
	$(INSTALL_DATA) $(LINUX_DIR)/../../bcmdrivers/broadcom/net/wl/bcm9$(BRCM_CHIP)/dhd/dongle/43602a1/rtecdc.bin $(1)/etc/wlan_dhd/43602a1
	$(INSTALL_DIR) $(1)/etc/wlan_dhd/mfg/43602a1
	$(INSTALL_DATA) $(LINUX_DIR)/../../bcmdrivers/broadcom/net/wl/bcm9$(BRCM_CHIP)/dhd/dongle/mfg/43602a1/rtecdc.bin $(1)/etc/wlan_dhd/mfg/43602a1
endef

$(eval $(call KernelPackage,bcm63xx-tch-wireless-dhd))

define KernelPackage/bcm63xx-tch-usb
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom USB driver
  DEPENDS:=@TARGET_brcm63xx_tch
  KCONFIG:=CONFIG_BCM_USB
  FILES:=$(LINUX_DIR)/../../bcmdrivers/broadcom/net/usb/bcm9$(BRCM_CHIP)/bcm_usb.ko
  AUTOLOAD:=$(call AutoLoad,57,bcm_usb)
endef

$(eval $(call KernelPackage,bcm63xx-tch-usb))

define KernelPackage/bcm63xx-tch-xtm
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom XTM Config driver
  DEPENDS:=@TARGET_brcm63xx_tch +(!PACKAGE_kmod-brcm-4.14L.04):kmod-bcm63xx-tch-xtmrtdrv
  KCONFIG:=CONFIG_BCM_XTMCFG CONFIG_ADSL_OS_OFFSET=18874368 CONFIG_ADSL_OS_RESERVED_MEM=1253376
  FILES:=$(LINUX_DIR)/../../bcmdrivers/broadcom/char/xtmcfg/bcm9$(BRCM_CHIP)/bcmxtmcfg.ko
  AUTOLOAD:=$(call AutoLoad,59,bcmxtmcfg)
endef

$(eval $(call KernelPackage,bcm63xx-tch-xtm))

define KernelPackage/bcm63xx-tch-xtmrtdrv
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom XTM RT driver
  DEPENDS:=@TARGET_brcm63xx_tch @(!PACKAGE_kmod-brcm-4.14L.04)
  KCONFIG:=CONFIG_BCM_XTMRT
  FILES:=$(LINUX_DIR)/../../bcmdrivers/opensource/net/xtmrt/bcm9$(BRCM_CHIP)/bcmxtmrtdrv.ko
  AUTOLOAD:=$(call AutoLoad,50,bcmxtmrtdrv)
endef

$(eval $(call KernelPackage,bcm63xx-tch-xtmrtdrv))

define KernelPackage/bcm63xx-tch-wfd
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom Wireless Forwarding Driver
  DEPENDS:=@PACKAGE_kmod-brcm-4.16L.02_voice4.16L.01||@PACKAGE_kmod-brcm-4.16L.02A||@PACKAGE_kmod-brcm-4.16L.03
  KCONFIG:=CONFIG_BCM_WIFI_FORWARDING_DRV CONFIG_BCM_WFD_CHAIN_SUPPORT=y
  FILES:=$(LINUX_DIR)/../../bcmdrivers/opensource/net/wfd/bcm9$(BRCM_CHIP)/wfd.ko
  AUTOLOAD:=$(call AutoLoad,56,wfd)
endef

$(eval $(call KernelPackage,bcm63xx-tch-wfd))

define KernelPackage/bcm63xx-tch-adsl
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom ADSL driver
  DEPENDS:=@TARGET_brcm63xx_tch
  KCONFIG:=CONFIG_BCM_ADSL
  FILES:=$(LINUX_DIR)/../../bcmdrivers/broadcom/char/adsl/bcm9$(BRCM_CHIP)/adsldd.ko
  AUTOLOAD:=$(call AutoLoad,60,adsldd)
endef

define KernelPackage/bcm63xx-tch-adsl/install
	$(INSTALL_DIR) $(1)/etc/adsl
ifneq ($(wildcard $(LINUX_DIR)/../../bcmdrivers/broadcom/char/adsl/bcm9$(BRCM_CHIP)/adsl_phy.bin),)
	$(INSTALL_DATA) $(LINUX_DIR)/../../bcmdrivers/broadcom/char/adsl/bcm9$(BRCM_CHIP)/adsl_phy.bin $(1)/etc/adsl
endif
ifneq ($(wildcard $(LINUX_DIR)/../../bcmdrivers/broadcom/char/adsl/bcm9$(BRCM_CHIP)/adsl_phy0.bin),)
	$(INSTALL_DATA) $(LINUX_DIR)/../../bcmdrivers/broadcom/char/adsl/bcm9$(BRCM_CHIP)/adsl_phy0.bin $(1)/etc/adsl
endif
ifneq ($(wildcard $(LINUX_DIR)/../../bcmdrivers/broadcom/char/adsl/bcm9$(BRCM_CHIP)/adsl_phy1.bin),)
	$(INSTALL_DATA) $(LINUX_DIR)/../../bcmdrivers/broadcom/char/adsl/bcm9$(BRCM_CHIP)/adsl_phy1.bin $(1)/etc/adsl
endif
endef

$(eval $(call KernelPackage,bcm63xx-tch-adsl))

define KernelPackage/bcm63xx-tch-pwrmngt
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom Power Management driver
  DEPENDS:=@TARGET_brcm63xx_tch
  KCONFIG:=CONFIG_BCM_PWRMNGT
  FILES:=$(LINUX_DIR)/../../bcmdrivers/broadcom/char/pwrmngt/bcm9$(BRCM_CHIP)/pwrmngtd.ko
  AUTOLOAD:=$(call AutoLoad,61,pwrmngtd)
endef

$(eval $(call KernelPackage,bcm63xx-tch-pwrmngt))

define KernelPackage/bcm63xx-tch-arl
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom ARL Table Management driver
  DEPENDS:=@TARGET_brcm63xx_tch
  KCONFIG:=CONFIG_BCM_ARL
  FILES:=$(LINUX_DIR)/../../bcmdrivers/broadcom/char/arl/bcm9$(BRCM_CHIP)/bcmarl.ko
  AUTOLOAD:=$(call AutoLoad,62,bcmarl)
endef

$(eval $(call KernelPackage,bcm63xx-tch-arl))

define KernelPackage/bcm63xx-tch-p8021ag
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom P8021AG driver
  DEPENDS:=@TARGET_brcm63xx_tch
  KCONFIG:=CONFIG_BCM_P8021AG
  FILES:=$(LINUX_DIR)/../../bcmdrivers/broadcom/char/p8021ag/bcm9$(BRCM_CHIP)/p8021ag.ko
  AUTOLOAD:=$(call AutoLoad,63,p8021ag)
endef

$(eval $(call KernelPackage,bcm63xx-tch-p8021ag))

define KernelPackage/bcm63xx-tch-endpoint
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom Endpoint driver
  DEPENDS:=@TARGET_brcm63xx_tch 
  KCONFIG:=CONFIG_BCM_ENDPOINT
  FILES:=$(LINUX_DIR)/../../bcmdrivers/broadcom/char/endpoint/bcm9$(BRCM_CHIP)/endpointdd.ko
  AUTOLOAD:=
endef

$(eval $(call KernelPackage,bcm63xx-tch-endpoint))

define KernelPackage/bcm63xx-tch-pcmshim
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom PCM SHIM driver
  DEPENDS:=@TARGET_brcm63xx_tch 
  KCONFIG:=CONFIG_BCM_PCMSHIM
  FILES:=$(LINUX_DIR)/../../bcmdrivers/opensource/char/pcmshim/bcm9$(BRCM_CHIP)/pcmshim.ko
  AUTOLOAD:=$(call AutoLoad,99,pcmshim)
endef

$(eval $(call KernelPackage,bcm63xx-tch-pcmshim))


define KernelPackage/bcm63xx-tch-dect
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom DECT driver
  DEPENDS:=@TARGET_brcm63xx_tch 
  KCONFIG:=CONFIG_BCM_DECT
  FILES:=$(LINUX_DIR)/../../bcmdrivers/broadcom/char/dect/bcm9$(BRCM_CHIP)/dect.ko
  AUTOLOAD:=$(call AutoLoad,99,dect)
endef

$(eval $(call KernelPackage,bcm63xx-tch-dect))


define KernelPackage/bcm63xx-tch-dectshim
  SUBMENU:=Broadcom specific kernel modules
  TITLE:=Broadcom DECTSHIM driver
  DEPENDS:=@TARGET_brcm63xx_tch 
  KCONFIG:=CONFIG_BCM_DECTSHIM
  FILES:=$(LINUX_DIR)/../../bcmdrivers/opensource/char/dectshim/bcm9$(BRCM_CHIP)/dectshim.ko
  AUTOLOAD:=$(call AutoLoad,99,dectshim)
endef

$(eval $(call KernelPackage,bcm63xx-tch-dectshim))

define KernelPackage/ipt-dyndscp-bcm63xx-tch
  SUBMENU:=Netfilter Extensions
  TITLE:=Broadcom DSCP inheritance from WAN
  DEPENDS:=@TARGET_brcm63xx_tch +kmod-ipt-conntrack
  KCONFIG:=CONFIG_NF_DYNDSCP
  FILES:=$(LINUX_DIR)/net/netfilter/nf_dyndscp.ko
  AUTOLOAD:=$(call AutoLoad,46,nf_dyndscp)
endef

$(eval $(call KernelPackage,ipt-dyndscp-bcm63xx-tch))

define KernelPackage/ipt-nathelper-rtsp-bcm63xx-tch
  SUBMENU:=Netfilter Extensions
  TITLE:=Broadcom kernel RTSP ALG
  DEPENDS:=@TARGET_brcm63xx_tch +kmod-ipt-conntrack +kmod-ipt-nat
  KCONFIG:=CONFIG_NF_CONNTRACK_RTSP CONFIG_NF_NAT_RTSP
  FILES:=$(LINUX_DIR)/net/netfilter/nf_conntrack_rtsp.ko $(LINUX_DIR)/net/ipv4/netfilter/nf_nat_rtsp.ko
  AUTOLOAD:=$(call AutoLoad,46,nf_conntrack_rtsp nf_nat_rtsp)
endef

$(eval $(call KernelPackage,ipt-nathelper-rtsp-bcm63xx-tch))

define KernelPackage/ipt-skiplog
  SUBMENU:=Netfilter Extensions
  TITLE:=Broadcom kernel xt SKIPLOG
  DEPENDS:=@TARGET_brcm63xx_tch||TARGET_brcm63xx_arm_tch
  KCONFIG:=CONFIG_NETFILTER_XT_TARGET_SKIPLOG
  FILES:=$(LINUX_DIR)/net/netfilter/xt_SKIPLOG.ko
  AUTOLOAD:=$(call AutoLoad,70,xt_SKIPLOG)
endef

$(eval $(call KernelPackage,ipt-skiplog))


define KernelPackage/brcm-4.16L.01
  SUBMENU:=Broadcom release
  TITLE:=Broadcom release 4.16L.01
  DEPENDS:=@TARGET_brcm63xx_tch
  KCONFIG:=CONFIG_BCM_WLAN_IMPL=16
  PROVIDES:=brcm-release
endef

$(eval $(call KernelPackage,brcm-4.16L.01))

define KernelPackage/brcm-4.14L.04
  SUBMENU:=Broadcom release
  TITLE:=Broadcom release 4.14L.04
  DEPENDS:=@TARGET_brcm63xx_tch
  KCONFIG:=CONFIG_BCM_WLAN_IMPL=16
  PROVIDES:=brcm-release
endef

$(eval $(call KernelPackage,brcm-4.14L.04))

define KernelPackage/brcm-4.16L.02_voice4.16L.01
  SUBMENU:=Broadcom release
  TITLE:=Broadcom release 4.16L.02 with voice on 4.16L.01
  DEPENDS:=@TARGET_brcm63xx_tch
  KCONFIG:=CONFIG_BCM_WLAN_IMPL=18
  PROVIDES:=brcm-release
endef

$(eval $(call KernelPackage,brcm-4.16L.02_voice4.16L.01))


define KernelPackage/brcm-4.16L.02A
  SUBMENU:=Broadcom release
  TITLE:=Broadcom release 4.16L.02A
  DEPENDS:=@TARGET_brcm63xx_tch
  KCONFIG:=CONFIG_BCM_WLAN_IMPL=18
  PROVIDES:=brcm-release
endef

$(eval $(call KernelPackage,brcm-4.16L.02A))

define KernelPackage/brcm-4.16L.03
  SUBMENU:=Broadcom release
  TITLE:=Broadcom release 4.16L.03
  DEPENDS:=@TARGET_brcm63xx_tch
  KCONFIG:=CONFIG_BCM_WLAN_IMPL=20
  PROVIDES:=brcm-release
endef

$(eval $(call KernelPackage,brcm-4.16L.03))
